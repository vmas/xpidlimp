﻿using System.IO;

namespace XpidlImp
{
	public interface IXpidlParser
	{
		XpidlFile Parse(TextReader xpidlTextReader);
	}
}
