﻿using System;

namespace XpidlImp
{
	public sealed class XpidlForwardDeclaration : XpidlNode
	{
		private readonly string _interfaceName;

		internal XpidlForwardDeclaration(string interfaceName)
		{
			_interfaceName = interfaceName;
		}

		public string InterfaceName
		{
			get { return _interfaceName; }
		}

		public override string ToString()
		{
			return string.Format("interface {0};", InterfaceName);
		}

		
	}
}
