﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Security;
using System.Text;
using System.Linq;
using System.Runtime.CompilerServices;

namespace XpidlImp.CodeDom
{
	internal static class CodeDomExtensions
	{
		public static string ToUpperCamel(this string s)
		{
			if (string.IsNullOrEmpty(s))
				return s;
			
			if (s.Length == 1)
				return s.ToUpper();

			return char.ToUpper(s[0]) + s.Substring(1);
		}

		public static bool Contains(this IEnumerable<XpidlModifier> @this, string name)
		{
			foreach(XpidlModifier modifier in @this)
			{
				if (modifier.Name == name)
					return true;
			}
			return false;
		}

		public static void AddVSDocComment(this CodeCommentStatementCollection codeCommentStatementCollection, XpidlComment comment)
		{
			if (comment != null)
			{

				codeCommentStatementCollection.Add(
					new CodeCommentStatement(
						string.Format("<summary>{0} {1}{0} </summary>", Environment.NewLine, SecurityElement.Escape(comment.Summary)),
						true)
				);
				foreach(KeyValuePair<string, string> param in comment.Params)
				{
					codeCommentStatementCollection.Add(
						new CodeCommentStatement(
							string.Format("<param name=\"{1}\">{0} {2}{0} </param>", Environment.NewLine, param.Key, SecurityElement.Escape(param.Value)),
							true)
					);
				}
				if (comment.Return != null)
				{
					codeCommentStatementCollection.Add(
						new CodeCommentStatement(
							string.Format("<returns>{0} {1}{0} </returns>", Environment.NewLine, SecurityElement.Escape(comment.Return)),
							true)
					);
				}
				string throwsText = "";
				foreach (KeyValuePair<string, string> param in comment.Throws)
				{
					throwsText += " " + param.Key + ": " + param.Value + Environment.NewLine;
				}
				if(!string.IsNullOrWhiteSpace(throwsText))
				{
					codeCommentStatementCollection.Add(
						new CodeCommentStatement(
							string.Format("<exception cref=\"System.Runtime.InteropServices.COMException\">{0} {1}{0} </exception>", Environment.NewLine, SecurityElement.Escape(throwsText.Trim())),
							true)
					);
				}
				if(comment.See != null)
				{
					codeCommentStatementCollection.Add(
						new CodeCommentStatement(
							string.Format("<remarks>{0} See {1}{0} </remarks>", Environment.NewLine, SecurityElement.Escape(comment.See)),
							true)
					);
				}
			}
		}

		public static void AddObsoleteAttribute(this CodeAttributeDeclarationCollection @this, XpidlComment comment)
		{
			if (comment != null && !string.IsNullOrEmpty(comment.Deprecated))
			{
				@this.Add(new CodeAttributeDeclaration(
					new CodeTypeReference(typeof(ObsoleteAttribute)),
					new CodeAttributeArgument(new CodePrimitiveExpression(
						comment.Deprecated.Replace('\n', ' ').Replace("\r", string.Empty).Replace("  ", " ")))
				));
			}
			else
			{
				@this.Add(new CodeAttributeDeclaration(
					new CodeTypeReference(typeof(ObsoleteAttribute))
				));
			}
		}

		public static void AddBeginRegion(this CodeTypeMemberCollection members, string text)
		{
			var snippet = new CodeSnippetTypeMember();
			snippet.StartDirectives.Add(new CodeRegionDirective(CodeRegionMode.Start, text));
			snippet.UserData["region"] = text;
			members.Add(snippet);
		}

		public static void AddEndRegion(this CodeTypeMemberCollection members)
		{
			var snippet = new CodeSnippetTypeMember();
			snippet.StartDirectives.Add(new CodeRegionDirective(CodeRegionMode.End, null));
			snippet.UserData["region"] = string.Empty;
			members.Add(snippet);
		}

		public static void AddComment(this CodeCommentStatementCollection codeCommentStatementCollection, string commentText)
		{
			if (commentText != null)
			{
				foreach (string comment in commentText.Split('\n'))
				{
					codeCommentStatementCollection.Add(new CodeCommentStatement(comment.Trim(), false));
				}
			}
		}

		public static void AddAttribute(this CodeAttributeDeclarationCollection codeAttributeDeclarationCollection, CodeAttributeDeclaration codeAttributeDeclaration)
		{
			if (codeAttributeDeclaration != null)
			{
				codeAttributeDeclarationCollection.Add(codeAttributeDeclaration);
			}
		}

		public static void AddMethodImplAttribute(this CodeAttributeDeclarationCollection codeAttributeDeclarationCollection)
		{
			var methodImplAttribute = new CodeAttributeDeclaration(
					new CodeTypeReference(typeof(MethodImplAttribute)),
					new CodeAttributeArgument(new CodeFieldReferenceExpression(new CodeTypeReferenceExpression(typeof(MethodImplOptions)), MethodImplOptions.InternalCall.ToString())),
					new CodeAttributeArgument("MethodCodeType", new CodeFieldReferenceExpression(new CodeTypeReferenceExpression(typeof(MethodCodeType)), MethodCodeType.Runtime.ToString()))
				);
			codeAttributeDeclarationCollection.Add(methodImplAttribute);
		}
	}
}
