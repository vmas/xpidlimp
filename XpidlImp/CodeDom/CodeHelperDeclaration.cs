﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XpidlImp.CodeDom
{
	class CodeHelperDeclaration
	{
		public CodeHelperDeclaration(string className, Guid uuid, string contractID)
		{
			this.ClassName = className;
			this.UUID = uuid;
			this.ContractID = contractID;
		}

		public string ClassName { get; private set; }

		public Guid UUID { get; private set; }

		public string ContractID { get; private set; }

	}
}
