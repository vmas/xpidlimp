﻿using System;
using System.ComponentModel;

namespace XpidlImp.CodeDom
{
	public sealed class CodeDomResolveXpidlType : HandledEventArgs
	{
		private readonly string _typeName;

		internal CodeDomResolveXpidlType(string typeName)
		{
			_typeName = typeName;
		}

		public string TypeName
		{
			get { return _typeName; }
		}
		public string ClrTypeName { get; set; }
		public Type ClrType { get; set; }
		public bool IsInterface { get; set; }
	}
}
