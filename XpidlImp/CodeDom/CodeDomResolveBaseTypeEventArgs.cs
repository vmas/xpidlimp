﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XpidlImp.CodeDom
{
	public class CodeDomResolveBaseTypeEventArgs : CodeDomTypeReferenceEventArgs
	{
		public CodeDomResolveBaseTypeEventArgs(string typeName, CodeNamespace nameSpace)
			: base(nameSpace)
		{
			TypeName = typeName;
			NameSpace = nameSpace;
		}

		public string TypeName { get; private set; }
		public string IdhFileName { get; set; }
	}
}
