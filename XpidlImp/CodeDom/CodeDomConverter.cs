﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Xml;
using System.Linq;
using System.Text;

namespace XpidlImp.CodeDom
{
	public class CodeDomConverter
	{
		private IDictionary<string, string> _idlMap;
		private IDictionary<string, string> _typeMap;
		private IDictionary<string, XpidlFile> _xpidlFiles;
		private ISet<string> _nonExistsFiles;

		private readonly string _language;
		private readonly string _srcPath;
		private readonly string _outPath;

		private static readonly UTF8Encoding UTF8WithoutBOM = new UTF8Encoding(false);

		public CodeDomConverter(string language, string srcPath, string outPath)
		{
			_language = language;
			_srcPath = srcPath;
			_outPath = outPath;
		}

		private IDictionary<string, string> IdlMap
		{
			get
			{
				if (_idlMap == null)
				{
					_idlMap = new Dictionary<string, string>();
					if (TypemapFile != null && File.Exists(TypemapFile))
					{
						using (var mapFile = File.OpenRead(TypemapFile))
						{
							using (var xml = XmlReader.Create(mapFile))
							{
								while (xml.Read())
								{
									if (xml.NodeType == XmlNodeType.Element
										&& xml.Name == "TypeContainer")
									{
										string typeName = xml.GetAttribute("type");
										string fileName = xml.GetAttribute("file");
										if (string.IsNullOrEmpty(typeName)
											|| string.IsNullOrEmpty(fileName))
										{
											Console.WriteLine("Invalid type map file!");
										}
										_idlMap[typeName] = fileName;
									}
								}
							}
						}
					}
				}
				return _idlMap;
			}
		}

		private IDictionary<string, string> TypeMap
		{
			get
			{
				if (_typeMap == null)
				{
					var typeMap = new Dictionary<string, string>();
					if (TypemapFile != null && File.Exists(TypemapFile))
					{
						using (var mapFile = File.OpenRead(TypemapFile))
						{
							using (var xml = XmlReader.Create(mapFile))
							{
								while (xml.Read())
								{
									if (xml.NodeType == XmlNodeType.Element
										&& xml.Name == "TypeReference")
									{
										string typeName = xml.GetAttribute("type");
										string clrName = xml.GetAttribute("clrtype");
										if (string.IsNullOrEmpty(typeName)
											|| string.IsNullOrEmpty(clrName))
										{
											Console.WriteLine("Invalid type map file!");
										}
										typeMap[typeName] = clrName;
									}
								}
							}
						}
					}
					_typeMap = typeMap;
				}
				return _typeMap;
			}
		}

		private IDictionary<string, XpidlFile> XpidlFiles
		{
			get { return _xpidlFiles ?? (_xpidlFiles = new Dictionary<string, XpidlFile>()); }
		}

		public ISet<string> NonExistsFiles
		{
			get { return _nonExistsFiles ?? (_nonExistsFiles = new HashSet<string>()); }
		}

		public string Language
		{
			get { return _language; }
		}

		public string InputPath
		{
			get { return _srcPath; }
		}

		public string OutputPath
		{
			get { return _outPath; }
		}

		public static string TypemapFile { get; set; }

		public IEnumerable<string> Imports { get; set; }

		public string TempPath
		{
			get { return Path.Combine(this.OutputPath, "intermediate"); }
		}

		public string Convert(string namespaceName, string filename)
		{
			bool generateHelper = "JS".Equals(this.Language, StringComparison.OrdinalIgnoreCase);
			// Create formatter
			IXpidlFormatter formatter = null;
			var codeGenOptions = new CodeGeneratorOptions
			{
				BlankLinesBetweenMembers = true,
				BracingStyle = "C",
				ElseOnClosing = false,
				IndentString = "\t",
				VerbatimOrder = true
			};
			if (generateHelper)
			{
				formatter = new CodeDomJSHelperFormatter(codeGenOptions);
			}
			else
			{
				formatter = new CodeDomXpidlFormatter(this.Language, codeGenOptions);
			}
			
			if (!Directory.Exists(this.TempPath))
				Directory.CreateDirectory(this.TempPath);

			string outFileName = Path.GetFileNameWithoutExtension(filename) + formatter.FileExtension;
			if(generateHelper && outFileName.StartsWith("nsI") && outFileName.Length > 3)
			{
				outFileName = outFileName.Substring(3);
			}
			outFileName = Path.Combine(this.OutputPath, outFileName);
			string iipFileName = Path.Combine(this.TempPath, Path.GetFileNameWithoutExtension(outFileName) + ".iip");
			if (File.Exists(outFileName) && File.Exists(iipFileName))
				return null;

			XpidlFile xpidlfile;
			var parser = new XPIDLParser();
#if DEBUG
			parser.Setup(Path.Combine("TreeBuilder", "xpidl.egt"));
#else
			parser.Setup("xpidl.egt");
#endif
			string fileFullPath = Path.Combine(this.InputPath, filename);
			if(!File.Exists(fileFullPath))
			{
				NonExistsFiles.Add(filename);
				return null;
			}
			string idlData = File.ReadAllText(fileFullPath);
			idlData = Regex.Replace(idlData, @"%}\s+?C\+\+", "%}");
			using (var file = new StringReader(idlData))
			{
				xpidlfile = parser.Parse(Path.GetFileName(filename), file);
				this.XpidlFiles[xpidlfile.FileName] = xpidlfile;
			}

			string componentDeclaration;
			var manifest = new StringBuilder();

			IDictionary<string, XpidlFile> xpidlCache = this.XpidlFiles;
			foreach(var include in xpidlfile.Includes)
			{
				XpidlFile incfile;
				if (xpidlCache.TryGetValue(include.FileName, out incfile))
				{
					include.XpidlFile = incfile;
				}
				else
				{
					Console.WriteLine("\tGenerating '{0}' ...", include.FileName);
					componentDeclaration = Convert(namespaceName, include.FileName);
					if (!string.IsNullOrEmpty(componentDeclaration))
						manifest.Append(componentDeclaration);
					
					if (xpidlCache.TryGetValue(include.FileName, out incfile))
					{
						include.XpidlFile = incfile;
					}	
				}
			}

			formatter.CodeNamespaceCreated += formatter_CodeNamespaceCreated;
			formatter.ResolveBaseType += formatter_ResolveBaseType;
			formatter.ResolveXpidlType += formatter_ResolveXpidlType;
			try
			{
				using (var file = new StreamWriter(outFileName, false, this.Language == "JS" ? UTF8WithoutBOM : System.Text.Encoding.UTF8))
				{
					formatter.Namespace = namespaceName;
					formatter.AddImports(this.Imports);
					componentDeclaration = formatter.Format(xpidlfile, file, Path.GetFileName(outFileName));
					if (!string.IsNullOrEmpty(componentDeclaration))
						manifest.Append(componentDeclaration);
					file.Flush();
				}
			}
			finally
			{
				formatter.ResolveBaseType -= formatter_ResolveBaseType;
				formatter.CodeNamespaceCreated -= formatter_CodeNamespaceCreated;
			}
			return manifest.ToString();
		}

		void formatter_ResolveXpidlType(object sender, CodeDomResolveXpidlType e)
		{
			string clrTypeName;
			if (e.IsInterface)
			{
				e.Handled = true;
				if(TypeMap.TryGetValue(e.TypeName, out clrTypeName))
				{
					e.IsInterface = false;
					Type t = Type.GetType(clrTypeName);
					if (t != null)
						e.ClrType = t;
					else
						e.ClrTypeName = clrTypeName;
				}
			}
			else
			{
				if (TypeMap.TryGetValue(e.TypeName, out clrTypeName))
				{
					e.Handled = true;
					Type t = Type.GetType(clrTypeName);
					if (t != null)
						e.ClrType = t;
					else
						e.ClrTypeName = clrTypeName;
				}
			}
		}

		void formatter_CodeNamespaceCreated(object sender, CodeDomTypeReferenceEventArgs e)
		{
			string sourceFile = ((CodeDomXpidlFormatter)sender).XpidlFile.FileName;
			string iipFile = Path.Combine(this.TempPath, Path.GetFileNameWithoutExtension(sourceFile) + ".iip");
			SerializeData(iipFile, e.NameSpace);
		}

		private void formatter_ResolveBaseType(object sender, CodeDomResolveBaseTypeEventArgs e)
		{
			var formatter = (CodeDomXpidlFormatter)sender;
			string rFile = Path.Combine(this.TempPath, e.TypeName + ".iip");
			if (!AddReferences(e.NameSpace, rFile))
			{
				string iipFileName = FindReference(formatter.Namespace, e.TypeName);
				if(iipFileName != null)
					AddReferences(e.NameSpace, iipFileName);
			}
		}

		private string FindReference(string namespaceName, string typeName)
		{
			string fileName;
			if (!IdlMap.TryGetValue(typeName, out fileName))
				fileName = typeName + ".idl";

			string iipFileName = Path.Combine(this.TempPath, Path.GetFileNameWithoutExtension(fileName) + ".iip");
			if (!File.Exists(iipFileName))
			{
				Console.WriteLine("\tGenerating '{0}' ...", fileName);
				Convert(namespaceName, fileName);
			}
			return iipFileName;
		}


		public bool AddReferences(CodeNamespace codeNamespace, string refFile)
		{
			CodeNamespace referencedNamespace = DeserializeData(refFile);
			if (referencedNamespace != null)
			{
				foreach (string key in referencedNamespace.UserData.Keys)
					codeNamespace.UserData[key] = referencedNamespace.UserData[key];
				return true;
			}
			return false;
		}

		/// ------------------------------------------------------------------------------------
		/// <summary>
		/// Serializes the data to a file with the same name as the IDL file but with the 
		/// extension ".iip".
		/// </summary>
		/// <param name="sFileName">Name of the IDL file.</param>
		/// <param name="cnamespace">The namespace definition with all classes and methods.</param>
		/// ------------------------------------------------------------------------------------
		private static void SerializeData(string fileName, CodeNamespace cnamespace)
		{
			using (FileStream outFile = new FileStream(fileName, FileMode.Create))
			{
				BinaryFormatter formatter = new BinaryFormatter();
				try
				{
					formatter.Serialize(outFile, cnamespace);
				}
				catch (SerializationException e)
				{
					Console.WriteLine("Failed to serialize to internal data file. Reason: {0}",
						e.Message);
				}
			}
		}

		/// ------------------------------------------------------------------------------------
		/// <summary>
		/// Deserializes the data.
		/// </summary>
		/// <param name="fileName">Name of the IIP file.</param>
		/// <returns>The namespace definition with all classes and methods.</returns>
		/// ------------------------------------------------------------------------------------
		private static CodeNamespace DeserializeData(string fileName)
		{
			if (!File.Exists(fileName))
				return null;

			using (FileStream inFile = new FileStream(fileName, FileMode.Open, FileAccess.Read))
			{
				BinaryFormatter formatter = new BinaryFormatter();
				try
				{
					object obj = formatter.Deserialize(inFile);
					return obj as CodeNamespace;
				}
				catch (SerializationException e)
				{
					Console.WriteLine(
						"Failed to deserialize referenced data from file \"{0}\". Reason: {1}",
						fileName, e.Message);
				}
			}
			return null;
		}

	}
}
