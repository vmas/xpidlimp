﻿using System;
using System.Linq;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Collections.Generic;

namespace XpidlImp.CodeDom
{
	public sealed partial class CodeDomXpidlFormatter : IXpidlFormatter
	{
		public event EventHandler<CodeDomResolveBaseTypeEventArgs> ResolveBaseType;
		public event EventHandler<CodeDomTypeReferenceEventArgs> CodeNamespaceCreated;
		public event EventHandler<CodeDomResolveXpidlType> ResolveXpidlType;

		private readonly string _lang;
		private readonly CodeGeneratorOptions _generatorOptions;
		private readonly string _fileExtension;

		public CodeDomXpidlFormatter(string language, CodeGeneratorOptions options)
		{
			if (language == null)
				throw new ArgumentNullException("language");

			if (options == null)
				throw new ArgumentNullException("options");

			if (!CodeDomProvider.IsDefinedLanguage(language))
				throw new ArgumentException("Language is not defined.", "language");

			string fileExtension = CodeDomProvider.GetCompilerInfo(language).GetExtensions().First();
			_fileExtension = fileExtension.StartsWith(".") ? fileExtension : ("." + fileExtension);

			_lang = language;
			_generatorOptions = options;
		}

		public void AddImports(IEnumerable<string> namespaces)
		{
			this.Imports.AddRange(namespaces);
		}

		public string Format(XpidlFile xpidlFile, TextWriter textWriter, string fileName)
		{
			var sb = new System.Text.StringBuilder();
			this.XpidlFile = xpidlFile;
			try
			{
				using (var w = new StringWriter(sb))
				{
					using (var codeDomProvider = CodeDomProvider.CreateProvider(Language))
					{
						CodeNamespace codeNamespace = CreateCodeNamespace(this.Namespace, xpidlFile);
						codeDomProvider.GenerateCodeFromNamespace(codeNamespace, w, Options);
						if (CodeNamespaceCreated != null)
							CodeNamespaceCreated(this, new CodeDomTypeReferenceEventArgs(codeNamespace));
					}
				}
			}
			finally
			{
				this.XpidlFile = null;
			}

			if ("C#".Equals(this.Language, StringComparison.CurrentCultureIgnoreCase))
			{
				textWriter.Write(
					@"#pragma warning disable 1591, 1572, 1573" + Environment.NewLine +
					Regex.Replace(Regex.Replace(sb.ToString(),
					@"(?<ch>[\[\(])System\.Runtime\.InteropServices\.", @"${ch}"),
					@"\[(?<name>.+?)Attribute\((?<args>.*?)\)\]", new MatchEvaluator((m) =>
				{
					string args = m.Groups["args"].Value;
					if (args.Length == 0)
						return "[" + m.Groups["name"].Value + "]";
					else
						return "[" + m.Groups["name"].Value + "(" + args + ")]";
				}))
				.Replace("System.Runtime.CompilerServices.", string.Empty)
				.Replace("[System.", "[").Replace("(1 << 31)", "(1U << 31)"));
			}
			else
			{
				textWriter.Write(sb.ToString());
			}
			return null;
		}

		public XpidlFile XpidlFile { get; private set; }
	
		public string FileExtension
		{
			get { return _fileExtension; }
		}

		public string Language
		{
			get { return _lang; }
		}

		public CodeGeneratorOptions Options
		{
			get { return _generatorOptions; }
		}

		public string Namespace { get; set; }

		/// ------------------------------------------------------------------------------------
		/// <summary>
		/// Returns a copy of the collection of the methods of the base type
		/// </summary>
		/// <param name="typeName">The name of the base type</param>
		/// <param name="nameSpace">The namespace which defines the base types</param>
		/// <param name="interfaceType">[out] Returns the interface type of the base type</param>
		/// <returns>A copy of the collection of the methods</returns>
		/// ------------------------------------------------------------------------------------
		private CodeTypeMemberCollection GetBaseMembers(string typeName,
			CodeNamespace nameSpace, out string interfaceType)
		{
			interfaceType = null;
			if (nameSpace.UserData[typeName] == null)
			{
				if (ResolveBaseType != null)
					ResolveBaseType(this, new CodeDomResolveBaseTypeEventArgs(typeName, nameSpace));
			}
			if (nameSpace.UserData[typeName] == null)
			{
				System.Console.WriteLine("Error: base type {0} not found!", typeName);
				return null;
			}
			else
			{

				CodeTypeDeclaration type = (CodeTypeDeclaration)nameSpace.UserData[typeName];
				interfaceType = (string)type.UserData["InterfaceType"];
				CodeTypeMemberCollection coll = new CodeTypeMemberCollection();
				
				//return type.Members;
				/// All base class members must be preceded by new
				foreach (CodeTypeMember member in type.Members)
				{
					CodeTypeMember newMember;
					if (member is CodeMemberMethod)
					{
						newMember = new CodeMemberMethod();
					}
					else if (member is CodeMemberProperty)
					{
						newMember = new CodeMemberProperty();
					}
					else if (member is CodeSnippetTypeMember && member.UserData["region"] != null)
					{
						string regionText = member.UserData["region"] as string;
						if (string.IsNullOrEmpty(regionText))
							coll.AddEndRegion();
						else
							coll.AddBeginRegion(regionText);
						continue;
					}
					else
					{
						Console.WriteLine("Unhandled member type: {0}", member.GetType());
						continue;
					}

					newMember.Attributes = member.Attributes | MemberAttributes.New;
					newMember.Attributes = newMember.Attributes & ~MemberAttributes.AccessMask |
						MemberAttributes.Public;
					newMember.Comments.AddRange(member.Comments);
					newMember.CustomAttributes.AddRange(member.CustomAttributes);
					newMember.Name = member.Name;
					if (member is CodeMemberMethod)
					{
						((CodeMemberMethod)newMember).ImplementationTypes.AddRange(((CodeMemberMethod)member).ImplementationTypes);
						((CodeMemberMethod)newMember).Parameters.AddRange(((CodeMemberMethod)member).Parameters);
						((CodeMemberMethod)newMember).ReturnType = ((CodeMemberMethod)member).ReturnType;
						((CodeMemberMethod)newMember).ReturnTypeCustomAttributes.AddRange(((CodeMemberMethod)member).ReturnTypeCustomAttributes);
					}
					else if (member is CodeMemberProperty)
					{
						((CodeMemberProperty)newMember).ImplementationTypes.AddRange(((CodeMemberProperty)member).ImplementationTypes);
						((CodeMemberProperty)newMember).Type = ((CodeMemberProperty)member).Type;
						((CodeMemberProperty)newMember).HasGet = ((CodeMemberProperty)member).HasGet;
						((CodeMemberProperty)newMember).HasSet = ((CodeMemberProperty)member).HasSet;
					}
					foreach (DictionaryEntry entry in member.UserData)
						newMember.UserData.Add(entry.Key, entry.Value);

					coll.Add(newMember);
				}

				return coll;
			}
		}
	}
}