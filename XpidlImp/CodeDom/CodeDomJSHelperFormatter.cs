﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XpidlImp.CodeDom
{
	sealed class CodeDomJSHelperFormatter : IXpidlFormatter
	{
		private CodeGeneratorOptions _codeGenOpts;

		public event EventHandler<CodeDomResolveBaseTypeEventArgs> ResolveBaseType;

		public event EventHandler<CodeDomTypeReferenceEventArgs> CodeNamespaceCreated;

		public event EventHandler<CodeDomResolveXpidlType> ResolveXpidlType;

		public CodeDomJSHelperFormatter(CodeGeneratorOptions codeGeneratorOptions)
		{
			_codeGenOpts = codeGeneratorOptions;
		}

		public string FileExtension
		{
			get { return ".js"; }
		}

		public string Namespace { get; set; }

		public void AddImports(IEnumerable<string> namespaces)
		{

		}

		public string Format(XpidlFile xpidlFile, TextWriter writer, string fileName)
		{
			writer.WriteLine("\"use strict\"");
			writer.WriteLine("const { classes: Cc, utils: Cu, interfaces: Ci, results: Cr } = Components;");
			writer.WriteLine("Cu.import(\"resource://gre/modules/XPCOMUtils.jsm\");");
			writer.WriteLine();

			var helpers = new List<CodeHelperDeclaration>();

			foreach (XpidlNode xpidlNode in xpidlFile.ChildNodes)
			{
				var xpidlInterface = xpidlNode as XpidlInterface;
				if (xpidlInterface == null)
					continue;

				helpers.Add(WriteInterface(writer, xpidlFile, xpidlInterface));
			}

			foreach (XpidlNode node in xpidlFile.ChildNodes)
			{
				
				var xpidlCode = node as XpidlInlineCHeader;
				if (xpidlCode == null)
					continue;

				writer.WriteLine();
				WriteRawCode(writer, xpidlCode);
			}
			writer.WriteLine();
			if (helpers.Count > 0)
			{
				writer.WriteLine("var NSGetFactory = XPCOMUtils.generateNSGetFactory([" + string.Join(", ", helpers.Select(decl => decl.ClassName)) + "]);");
			}

			var manifestBuilder = new StringBuilder();
			foreach(CodeHelperDeclaration helper in helpers)
			{
				string iid = helper.UUID.ToString("B");
				manifestBuilder.Append("component ")
					.Append(iid)
					.Append(" ");
				if (!string.IsNullOrEmpty(this.Namespace))
					manifestBuilder.Append(this.Namespace).Append('/');
				manifestBuilder.AppendLine(fileName);

				manifestBuilder.Append("contract ")
					.Append(helper.ContractID)
					.Append(" ")
					.AppendLine(iid);
				manifestBuilder.AppendLine();
			}

			return manifestBuilder.ToString();
		}

		private string CreateContractByClassName(string className)
		{
			bool isSmall = true;
			var sb = new StringBuilder(className.Length);
			List<string> data = new List<string>(3);
			foreach(char ch in className)
			{
				if(Char.IsUpper(ch))
				{
					if(sb.Length > 0 && isSmall)
					{
						data.Add(sb.ToString().ToLowerInvariant());
						sb.Clear();
					}
					isSmall = false;
				}
				else if(!isSmall && sb.Length > 1)
				{
					char nameStart = sb[sb.Length - 1];
					data.Add(sb.Remove(sb.Length - 1, 1).ToString().ToLowerInvariant());
					sb.Clear().Append(nameStart);
					
					isSmall = true;
				}
				else
				{
					isSmall = true;
				}
				sb.Append(ch);
			}
			if (sb.Length > 0)
			{
				className = sb.ToString().ToLowerInvariant();
				if (data.Count <= 2 && className != "helper")
					data.Add(className);
			}
			return "@" + string.Join("/", data.Take(2)) + "/" + string.Join("-", data.Skip(2)) + ";1";
		}

		private CodeHelperDeclaration WriteInterface(TextWriter writer, XpidlFile xpidlFile, XpidlInterface @interface)
		{
			Guid uuid = Guid.NewGuid();
			string className = @interface.Name.Substring(3);
			string classContractName = "NS_" + className.ToUpperInvariant() + "_CONTRACTID";

			foreach(XpidlInlineCHeader node in xpidlFile.ChildNodes.OfType<XpidlInlineCHeader>())
			{
				int startPos = node.HeaderText.IndexOf(classContractName);
				if (startPos == -1)
					continue;
				startPos = node.HeaderText.IndexOf('"', startPos) + 1;
				if (startPos == 0)
					continue;

				int endPos = node.HeaderText.IndexOf('"', startPos);
				if (endPos == -1)
					continue;

				classContractName = node.HeaderText.Substring(startPos, endPos - startPos);
				break;
			}
			if (classContractName.EndsWith("_CONTRACTID"))
			{
				classContractName = CreateContractByClassName(className);
			}

			writer.WriteLine("function " + className + "() {");
			writer.WriteLine("\tthis.instance = null");
			writer.WriteLine("}");
			writer.Write(className);
			writer.WriteLine(".prototype = {");
			writer.WriteLine("\tclassDescription: \"" + className + "\",");
			writer.WriteLine("\tclassID: Components.ID(\"" + uuid.ToString("B") + "\"),");
			writer.WriteLine("\tcontractID: \"" + classContractName + "\",");
			writer.WriteLine("\tQueryInterface: XPCOMUtils.generateQI([Ci." + @interface.Name + "]),");
			writer.WriteLine();

			foreach(XpidlNode node in @interface.ChildNodes)
			{
				var xpidlAttribute = node as XpidlAttribute;
				if (xpidlAttribute != null)
				{
					WriteAttribute(writer, xpidlAttribute);
					continue;
				}
				var xpidlMethod = node as XpidlMethod;
				if (xpidlMethod != null)
				{
					WriteMethod(writer, xpidlMethod);
					continue;
				}
			}
			writer.WriteLine("}");
			writer.WriteLine();
			return new CodeHelperDeclaration(className, uuid, classContractName);
		}

		private static IEnumerable<string> GetJavaScriptCode(string code, string boundary = "#JavaScript#")
		{
			bool canWrite = false;
			foreach (string line in code.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries))
			{
				if (line.Contains(boundary))
				{
					canWrite = !canWrite;
					continue;
				}
				if (!canWrite)
				{
					continue;
				}
				yield return line.Trim();
			}
		}

		private void WriteFormattedLines(TextWriter writer, IEnumerable<string> lines, int startTabs)
		{
			string tabs = new string('\t', startTabs);
			foreach (string line in lines)
			{
				if (line.StartsWith("}") && tabs.Length > 0)
				{
					tabs = tabs.Remove(tabs.Length - 1);
				}
				writer.Write(tabs);
				writer.WriteLine(line);
				if (line.EndsWith("{"))
				{
					tabs += "\t";
				}
			}
		}

		private void WriteRawCode(TextWriter writer, XpidlInlineCHeader xpidlCode)
		{
			WriteFormattedLines(writer, GetJavaScriptCode(xpidlCode.HeaderText), 0);
		}

		private void WriteAttribute(TextWriter writer, XpidlAttribute attribute)
		{
			string[] getterCode = null;
			string[] setterCode = null;
			XpidlInlineCHeader xpidlCode = attribute.ChildNodes.OfType<XpidlInlineCHeader>().FirstOrDefault();
			if (xpidlCode != null)
			{
				string code = string.Join("\n", GetJavaScriptCode(xpidlCode.HeaderText));
				if (!string.IsNullOrWhiteSpace(code))
				{
					getterCode = GetJavaScriptCode(code, "#get#").ToArray();
					if (getterCode.Length == 0)
						throw new FormatException(string.Format("Invalid getter for '{0}'!", attribute.Name));

					if (!attribute.IsReadOnly)
					{
						setterCode = GetJavaScriptCode(code, "#set#").ToArray();
						if (setterCode.Length == 0)
							throw new FormatException(string.Format("Invalid setter for '{0}'!", attribute.Name));
					}
				}
			}
			writer.Write("\tget ");
			writer.Write(attribute.Name);
			writer.WriteLine("() {");
			if (getterCode != null)
			{
				WriteFormattedLines(writer, getterCode, 2);
			}
			else
			{
				writer.Write("\t\treturn this.instance.");
				writer.WriteLine(attribute.Name);
			}
			writer.WriteLine("\t},");

			if (!attribute.IsReadOnly)
			{
				writer.Write("\tset ");
				writer.Write(attribute.Name);
				writer.WriteLine("(value) {");
				if (setterCode != null)
				{
					WriteFormattedLines(writer, setterCode, 2);
				}
				else
				{
					writer.Write("\t\tthis.instance.");
					writer.Write(attribute.Name);
					writer.WriteLine(" = value");
				}
				writer.WriteLine("\t},");


			}
		}

		private void WriteMethod(TextWriter writer, XpidlMethod method)
		{
			string[] methodCode = null;
			XpidlInlineCHeader xpidlCode = method.ChildNodes.OfType<XpidlInlineCHeader>().FirstOrDefault();
			if (xpidlCode != null)
			{
				string code = string.Join("\n", GetJavaScriptCode(xpidlCode.HeaderText));
				if (!string.IsNullOrWhiteSpace(code))
				{
					methodCode = GetJavaScriptCode(code, "#get#").ToArray();
					if (methodCode.Length == 0)
						throw new FormatException(string.Format("Invalid method body for '{0}'!", method.Name));
				}
			}
			string args = string.Join(", ", method.Parameters.Select(p => p.Name));

			writer.Write("\t");
			writer.Write(method.Name);
			writer.Write(": function(");
			writer.Write(args);
			writer.WriteLine(") {");
			if (methodCode != null)
			{
				WriteFormattedLines(writer, methodCode, 2);
			}
			else if(method.Name == "init")
			{
				writer.Write("\t\tthis.instance = ");
				writer.Write(method.Parameters.First().Name);
				writer.WriteLine();
			}
			else
			{
				writer.Write(method.Type != "void" ? "\t\treturn " : "\t\t");
				writer.Write("this.instance.");
				writer.Write(method.Name);
				writer.Write("(");
				writer.Write(args);
				writer.WriteLine(")");
			}
			writer.WriteLine("\t},");
		}

	}
}
