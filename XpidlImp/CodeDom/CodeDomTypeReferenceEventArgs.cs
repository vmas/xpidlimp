﻿using System;
using System.CodeDom;

namespace XpidlImp.CodeDom
{
	public class CodeDomTypeReferenceEventArgs : EventArgs
	{
		public CodeDomTypeReferenceEventArgs(CodeNamespace nameSpace)
		{
			NameSpace = nameSpace;
		}

		public CodeNamespace NameSpace { get; set; }
	}
}
