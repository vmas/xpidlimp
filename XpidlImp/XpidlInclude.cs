﻿using System;

namespace XpidlImp
{
	public sealed class XpidlInclude : XpidlNode
	{
		private readonly string _fileName;

		internal XpidlInclude(string fileName)
		{
			_fileName = fileName;
		}

		public string FileName
		{
			get { return _fileName; }
		}

		public override string ToString()
		{
			return string.Format("#include \"{0}\"", FileName);
		}

		public XpidlFile XpidlFile { get; set; }
	}
}
