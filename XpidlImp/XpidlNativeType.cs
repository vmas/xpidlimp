﻿using System;

namespace XpidlImp
{
	public sealed class XpidlNativeType : XpidlNode
	{
		private readonly string _name;
		private readonly string _definition;
		private readonly string[] _modifiers;

		internal XpidlNativeType(string name, string definition, string[] modifiers)
		{
			_name = name;
			_definition = definition;
			_modifiers = modifiers;
		}

		public String Name
		{
			get { return _name; }
		}

		public String Definition
		{
			get { return _definition; }
		}

		public string Comment { get; internal set; }

		public string[] Modifiers
		{
			get { return _modifiers; }
		}

		public override string ToString()
		{
			string modifiers = string.Join(", ", Modifiers);
			return (modifiers.Length > 0 ? "[" + modifiers + "] " : string.Empty) + string.Format("native {0} ({1});", Name, Definition);
		}

	}
}
