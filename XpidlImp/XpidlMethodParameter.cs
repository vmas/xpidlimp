﻿using System;

namespace XpidlImp
{
	[Flags]
	public enum XpidlParameterDirection : byte
	{
		Default = 0x00,
		In = 0x01,
		Out = 0x02
	}

	public sealed class XpidlMethodParameter
	{
		private readonly string _name;
		private string _type;
		private readonly XpidlParameterDirection _direction;
		private readonly XpidlModifier[] _modifiers;

		internal XpidlMethodParameter(string name, string type, XpidlParameterDirection direction, XpidlModifier[] modifiers)
		{
			_name = name;
			_type = type;
			_direction = direction;
			_modifiers = modifiers;
		}

		public string Name
		{
			get { return _name; }
		}

		public string Type
		{
			get { return _type; }
			internal set { _type = value; }
		}

		public XpidlParameterDirection Direction
		{
			get { return _direction; }
		}

		public XpidlModifier[] Modifiers
		{
			get { return _modifiers; }
		}
	}
}
