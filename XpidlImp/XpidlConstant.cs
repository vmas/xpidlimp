﻿using System;
using System.Linq.Expressions;

namespace XpidlImp
{
	public sealed class XpidlConstant : XpidlNamedNode
	{
		private readonly Expression _value;

		internal XpidlConstant(string name, string type, Expression value)
			: base(name, type)
		{
			_value = value;
		}

		public Expression Value
		{
			get { return _value; }
		}

	}
}
