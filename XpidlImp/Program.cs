﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using XpidlImp.CodeDom;

namespace XpidlImp
{
	static class Program
	{
		private static void ShowHelp()
		{
			string assemblyLocation = Assembly.GetEntryAssembly().Location;
			Console.WriteLine("\nXPIDL importer: creates .NET interfaces from an IDL file.");
			Console.WriteLine("Copyright (C) 2014. All Rights Reserved.\n");
			Console.WriteLine("Syntax:\n");
			Console.WriteLine("{0} [options] path | xpidlfile.idl", Path.GetFileName(assemblyLocation));
			Console.WriteLine();
			Console.WriteLine("possible options:");
			Console.WriteLine("\t/o outpath\toutput directory (Default: same as source path)");
			Console.WriteLine("\t/c configfile\tname of XML configuration file ({0}.xml)",
				Path.GetFileNameWithoutExtension(assemblyLocation));
			Console.WriteLine("\t/m mapfile\ttypename map file (map.xml)");
			Console.WriteLine("\t/M manifest\toutput file for chrome registration (chrome.manifest)");
			Console.WriteLine("\t/n namespace\tNamespace of the file to be produced");
			Console.WriteLine("\t/u namespace\tadditional using namespaces");
			Console.WriteLine("\t/r iipfile\tFile name of .iip file to use to resolve references");
			Console.WriteLine("\t/x (0|1)\t1 = create, 0 = suppress XML comments (Default: 1)");
			Console.WriteLine("\t/L (C#|VB|JS)\tC# or VB - generate COM-interfaces (Default: C#)\n\t\t\tJS - generate JavaScript helper class");
			Console.WriteLine("\t/? \t\tshow this help information");
			Console.ReadKey();
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static int Main(string[] args)
		{
			if (args.Length < 1 || args.Length == 1 && (args[0] == "/?" || args[0] == "-?"))
			{
				ShowHelp();
				return 0;
			}

			string tempPath = null;
			try
			{
				string sourcePath;
				string typemapFile = "map.xml";
				string chromeManifestFile = null;

				string sourceFile = Path.GetFullPath(args[args.Length - 1]);
				//sourceFile = @"G:\idl";
				
				if (Directory.Exists(sourceFile))
				{
					sourcePath = sourceFile;
					sourceFile = null;
				}
				else if (File.Exists(sourceFile) && sourceFile.EndsWith(".idl", StringComparison.OrdinalIgnoreCase))
				{
					sourcePath = Path.GetDirectoryName(sourceFile);
				}
				else
				{
					Console.WriteLine("Wrong path: '{0}'!", sourceFile);
					return 1;
				}
				string outputPath = sourcePath;
				string sXmlFile = null;
				string sNamespace = null;
				string language = "C#";
				// Get all necessary file names
				List<string> usingNamespaces = new List<string>();
				StringCollection refFiles = new StringCollection();
				bool fCreateComments = true;

				for (int i = 0; i < (args.Length - 1) / 2; i++)
				{
					switch (args[i * 2])
					{
						case "/o":
						case "-o":
							outputPath = args[i * 2 + 1];
							break;
						case "/c":
						case "-c":
							sXmlFile = args[i * 2 + 1];
							break;
						case "/m":
						case "-m":
							typemapFile = args[i * 2 + 1];
							break;
						case "/M":
						case "-M":
							chromeManifestFile = args[i * 2 + 1];
							break;
						case "/n":
						case "-n":
							sNamespace = args[i * 2 + 1];
							break;
						case "/u":
						case "-u":
							usingNamespaces.Add(args[i * 2 + 1]);
							break;
						case "/?":
						case "-?":
							ShowHelp();
							return 0;
						case "/x":
						case "-x":
							fCreateComments = (args[i * 2 + 1] == "0" ? false : true);
							break;
						case "/r":
						case "-r":
							refFiles.Add(args[i * 2 + 1]);
							break;
						case "/L":
						case "-L":
							language = args[i * 2 + 1].ToUpperInvariant();
							if(language != "C#" && language != "VB" && language != "JS")
							{
								Console.WriteLine("\nWrong language: {0}\n", language);
								ShowHelp();
								return 0;
							}
							break;
						default:
							Console.WriteLine("\nWrong parameter: {0}\n", args[i * 2]);
							ShowHelp();
							return 0;
					}
				}

				if (!Directory.Exists(outputPath))
					Directory.CreateDirectory(outputPath);

				if (chromeManifestFile == null)
					chromeManifestFile = Path.Combine(outputPath, "chrome.manifest");

				CodeDomConverter.TypemapFile = Path.GetFullPath(typemapFile);
				if(!File.Exists(CodeDomConverter.TypemapFile))
				{
					Console.WriteLine("File not found: ", CodeDomConverter.TypemapFile);
					return 1;
				}

				string manifestDeclaration = null;
				var manifest = new StringBuilder();
				var converter = new CodeDomConverter(language, sourcePath, outputPath);
				converter.Imports = usingNamespaces;

				tempPath = converter.TempPath;
				if (sourceFile != null)
				{
					manifestDeclaration = converter.Convert(sNamespace, Path.GetFileName(sourceFile));
					if (!string.IsNullOrWhiteSpace(manifestDeclaration))
						manifest.Append(manifestDeclaration);
				}
				else
				{
					string[] files = Directory.EnumerateFiles(sourcePath, "*.idl").ToArray();
					int count = files.Length;
					for (int i = 0; i < count; i++)
					{
						string fileName = Path.GetFileName(files[i]);
						Console.WriteLine("[{0} / {1}] Generating '{2}' ...", i + 1, count, fileName);
						manifestDeclaration = converter.Convert(sNamespace, fileName);
						if (!string.IsNullOrWhiteSpace(manifestDeclaration))
							manifest.Append(manifestDeclaration);
					}
				}
				if(converter.NonExistsFiles.Count > 0)
				{
					Console.WriteLine("Missing files:");
					foreach(string file in converter.NonExistsFiles)
					{
						Console.WriteLine("\t" + file);
					}
				}

				if(manifest.Length > 0)
				{
					File.WriteAllText(chromeManifestFile, manifest.ToString(), Encoding.ASCII);
				}




			}
			//catch (Exception e)
			//{
			//	System.Console.WriteLine("Internal program error in program {0}", e.Source);
			//	System.Console.WriteLine("\nDetails:\n{0}\nin method {1}.{2}\nStack trace:\n{3}",
			//		e.Message, e.TargetSite.DeclaringType.Name, e.TargetSite.Name, e.StackTrace);

			//	return 1;
			//}
			finally
			{
				if (tempPath != null && Directory.Exists(tempPath))
					Directory.Delete(tempPath, true);
			}


			return 0;
		}





	}
}
