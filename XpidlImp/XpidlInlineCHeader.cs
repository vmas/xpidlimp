﻿using System;

namespace XpidlImp
{
	public sealed class XpidlInlineCHeader : XpidlNode
	{
		private readonly string _headerText;

		internal XpidlInlineCHeader(string headerText)
		{
			_headerText = headerText;
		}

		public string HeaderText
		{
			get { return _headerText; }
		}

	}
}
