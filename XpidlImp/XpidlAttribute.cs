﻿using System;

namespace XpidlImp
{
	public sealed class XpidlAttribute : XpidlNamedNode
	{
		private readonly bool _isReadOnly;
		private readonly XpidlModifier[] _modifiers;

		internal XpidlAttribute(string name, string type, bool isReadOnly, XpidlModifier[] modifiers)
			: base(name, type)
		{
			_isReadOnly = isReadOnly;
			_modifiers = modifiers;
		}

		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		public XpidlModifier[] Modifiers
		{
			get { return _modifiers; }
		}

		public override string ToString()
		{
			return string.Format("{0}attribute {1} {2};", IsReadOnly ? "readonly " : string.Empty, Type, Name);
		}


	}
}
