﻿using System.Collections.Generic;
using System.Linq;

namespace XpidlImp
{
	public sealed class XpidlFile : XpidlComplexNode
	{
		private Dictionary<string, XpidlTypeDef> _typeDefs = new Dictionary<string, XpidlTypeDef>();
		private Dictionary<string, XpidlNativeType> _nativeDefs = new Dictionary<string, XpidlNativeType>();

		internal XpidlFile(string fileName)
		{
			this.FileName = fileName;
		}

		public string FileName { get; private set; }

		internal void AddNode(XpidlNode node)
		{
			AddNodeImpl(node);
		}

		internal void AddTypeDef(XpidlTypeDef typeDef)
		{
			_typeDefs[typeDef.Name] = typeDef;
			AddNodeImpl(typeDef);
		}

		internal void AddNativeType(XpidlNativeType nativeType)
		{
			_nativeDefs[nativeType.Name] = nativeType;
			AddNodeImpl(nativeType);
		}

		internal void AddInclude(XpidlInclude @include)
		{
			AddNodeImpl(@include);
		}

		internal void AddForwardDeclaration(XpidlForwardDeclaration fwDecl)
		{
			AddNodeImpl(fwDecl);
		}

		public IEnumerable<XpidlInclude> Includes
		{
			get
			{
				return this.ChildNodes.OfType<XpidlInclude>();
			}
		}

		public string ResolveTypeDefinition(string name)
		{
			XpidlTypeDef typeDef;
			if (_typeDefs.TryGetValue(name, out typeDef))
				return typeDef.Type;
			foreach(var @include in this.Includes)
			{
				XpidlFile includedFile = @include.XpidlFile;
				if(includedFile != null)
				{
					string type = includedFile.ResolveTypeDefinition(name);
					if (type != null)
						return type;
				}
			}
			return null;
		}

		public XpidlNativeType ResolveNativeType(string name)
		{
			XpidlNativeType nativeType;
			if (_nativeDefs.TryGetValue(name, out nativeType))
				return nativeType;
			foreach (var @include in this.Includes)
			{
				XpidlFile includedFile = @include.XpidlFile;
				if (includedFile != null)
				{
					XpidlNativeType type = includedFile.ResolveNativeType(name);
					if (type != null)
						return type;
				}
			}
			return null;
		}


	}
}
