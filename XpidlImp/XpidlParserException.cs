﻿using System;

namespace XpidlImp
{
	public class XpidlParserException : Exception
	{
		internal XpidlParserException(string message)
			: base(message)
		{

		}

		internal XpidlParserException(string message, Exception innerException)
			: base(message, innerException)
		{

		}
	}

	public class XpidlParserSyntaxException : XpidlParserException
	{
		private readonly string _tokenText;
		private readonly int _line;
		private readonly int _position;

		internal XpidlParserSyntaxException(string message, string tokenText, int line, int position)
			: base(message)
		{
			_tokenText = tokenText;
			_line = line;
			_position = position;
		}

		public string TokenText
		{
			get { return _tokenText; }
		}

		public int Line
		{
			get { return _line; }
		}

		public int Position
		{
			get { return _position; }
		}

	}
}
