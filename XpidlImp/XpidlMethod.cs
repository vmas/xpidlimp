﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace XpidlImp
{


	public sealed class XpidlMethod : XpidlNamedNode
	{

		private readonly XpidlModifier[] _modifiers;
		private readonly ReadOnlyCollection<XpidlMethodParameter> _parameters;
		private readonly ReadOnlyCollection<string> _exceptions;

		internal XpidlMethod(string name, string type, XpidlModifier[] modifiers, IEnumerable<XpidlMethodParameter> parameters, IEnumerable<String> exceptions)
			: base(name, type)
		{
			_modifiers = modifiers;
			_parameters = (parameters != null ? new List<XpidlMethodParameter>(parameters) : new List<XpidlMethodParameter>(0)).AsReadOnly();
			_exceptions = (exceptions != null ? new List<string>(exceptions) : new List<string>(0)).AsReadOnly();
		}

		public XpidlModifier[] Modifiers
		{
			get { return _modifiers; }
		}

		public IList<XpidlMethodParameter> Parameters
		{
			get { return _parameters; }
		}

		public IList<string> Exceptions
		{
			get { return _exceptions; }
		}

		public int GetParameterIndex(string parameterName)
		{
			for (int i = 0; i < Parameters.Count; ++i)
			{
				if (Parameters[i].Name == parameterName)
				{
					return i;
				}
			}
			return -1;
		}


	}
}
