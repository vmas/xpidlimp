﻿using System;
using System.Collections.Generic;
using System.IO;
using XpidlImp.CodeDom;

namespace XpidlImp
{
	public interface IXpidlFormatter
	{
		event EventHandler<CodeDomResolveBaseTypeEventArgs> ResolveBaseType;
		event EventHandler<CodeDomTypeReferenceEventArgs> CodeNamespaceCreated;
		event EventHandler<CodeDomResolveXpidlType> ResolveXpidlType;

		string FileExtension { get; }
		string Namespace { get; set; }
		
		void AddImports(IEnumerable<string> namespaces);
		string Format(XpidlFile xpidlFile, TextWriter writer, string fileName);
	}
}
