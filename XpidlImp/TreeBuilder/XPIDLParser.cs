﻿using GOLD;
using System.IO;
using System.Linq;

namespace XpidlImp
{
	partial class XPIDLParser
	{
	    private Parser parser = new Parser(); 
	
	    public void Setup(string grammarPath = "grammar.egt")
	    {
	        //This procedure can be called to load the parse tables. The class can
	        //read tables using a BinaryReader.
	        parser.LoadTables(Path.GetFullPath(grammarPath));
	    }
	    
	    public XpidlFile Parse(string fileName, TextReader reader)
	    {
	        //This procedure starts the GOLD Parser Engine and handles each of the
	        //messages it returns. Each time a reduction is made, you can create new
	        //custom object and reassign the .CurrentReduction property. Otherwise, 
	        //the system will use the Reduction object that was returned.
	        //
	        //The resulting tree will be a pure representation of the language 
	        //and will be ready to implement.
	
	        ParseMessage response; 
	        bool done;                      //Controls when we leave the loop
	
	        parser.Open(reader);
	        parser.TrimReductions = false;  //Please read about this feature before enabling  



	        done = false;
	        while (!done)
	        {
	            response = parser.Parse();

	            switch (response)
	            {
	                case ParseMessage.LexicalError:
	                    //Cannot recognize token
	                    done = true;
	                    break;
	
	                case ParseMessage.SyntaxError:
	                    //Expecting a different token
	                    done = true;
	                    break;
	
				
	                case ParseMessage.Reduction:
	                    break;
	
	                case ParseMessage.Accept:
						return BuildFile(fileName, (Reduction)parser.CurrentReduction);
	                case ParseMessage.TokenRead:
	                    //You don't have to do anything here.
	                    break;
	
	                case ParseMessage.InternalError:
	                    //INTERNAL ERROR! Something is horribly wrong.
	                    done = true;
	                    break;
	
	                case ParseMessage.NotLoadedError:
	                    //This error occurs if the CGT was not loaded.                   
	                    done = true;
	                    break;
	
	                case ParseMessage.GroupError: 
	                    //GROUP ERROR! Unexpected end of file
	                    done = true;
	                    break;
	            } 
	        } //while
	
	        return null;
	    }


	    
	}
}
