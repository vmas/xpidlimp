﻿using GOLD;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace XpidlImp
{
	static class XPIDLParserExtensions
	{
		public static IEnumerable<Reduction> GetAll(this Reduction r, params string[] rulesNames)
		{
			bool any = rulesNames.Length == 0;
			for (int i = 0, count = r.Count(); i < count; i++)
			{
				Reduction reduction = r.get_Data(i) as Reduction;
				if (reduction != null)
				{
					if (any || rulesNames.Contains(reduction.Parent.Head().Name()))
						yield return reduction;
					foreach (var rd in GetAll(reduction, rulesNames))
						yield return rd;
				}
			}
		}

		public static IEnumerable<Reduction> GetSiblings(this Reduction r, params string[] rulesNames)
		{
			for (int i = 0, count = r.Count(); i < count; i++)
			{
				Reduction reduction = r.get_Data(i) as Reduction;
				if (reduction != null)
				{
					if (rulesNames.Contains(reduction.Parent.Head().Name()))
						yield return reduction;
				}
			}
		}

		public static IEnumerable<object> GetSiblings(this Reduction r)
		{
			for (int i = 0, count = r.Count(); i < count; i++)
			{
				yield return r.get_Data(i);
			}
		}

	}

	partial class XPIDLParser
	{

		private static Reduction CastWithRule(object reduction, string ruleName)
		{
			var r = reduction as Reduction;
			if (r != null && r.Parent.Head().Name() == ruleName)
			{
				return r;
			}
			throw new XpidlParserException(string.Format("Invalid syntax: '{0}' expected", ruleName));
		}

		private static XpidlFile BuildFile(string fileName, Reduction r)
		{
			var file = new XpidlFile(fileName);
			foreach(var reduction in r.GetAll())
			{
				string itemName = reduction.Parent.Head().Name();
				switch(itemName)
				{
					case "xpidl-interface-decl":
						file.AddNode(BuildInterface(reduction));
						break;
					case "xpidl-typedef":
						file.AddTypeDef(BuildTypeDef(reduction));
						break;
					case "xpidl-native-type-decl":
						file.AddNativeType(BuildNativeType(reduction));
						break;
					case "xpidl-include":
						file.AddInclude(new XpidlInclude((string)reduction.get_Data(2)));
						break;
					case "xpidl-forward-declaration":
						file.AddForwardDeclaration(new XpidlForwardDeclaration((string)reduction.get_Data(1)));
						break;
				}
			}
			foreach (var reduction in r.GetAll("xpidl-item"))
			{
				var reduct = reduction.get_Data(0) as Reduction;
				if (reduct != null && reduct.Parent.Head().Name() == "xpidl-comment-list")
				{
					foreach (var reduction2 in reduct.GetAll("xpidl-comment"))
					{
						if (reduction2.Parent.Handle().Text() == "RawCode")
						{
							string code = (string)reduction2.get_Data(0);
							file.AddNode(new XpidlInlineCHeader(code));
						}
					}
				}
			}
			return file;
		}

		private static string GetComment(Reduction root)
		{
			string comment = null;
			Reduction commentsReduction = root.GetSiblings("xpidl-comment-list").FirstOrDefault();
			if (commentsReduction != null)
			{
				comment = (string)commentsReduction.GetAll("xpidl-comment").Last().get_Data(0);
				if (!comment.StartsWith("/*"))
					comment = null;
			}
			return comment;
		}

		private static XpidlNativeType BuildNativeType(Reduction root)
		{
			Reduction ntype = root.GetAll("xpidl-native-type").First();
			var nativeType = new XpidlNativeType(
				(string)root.GetAll("xpidl-native-type-id").First().get_Data(0),
				"...", // TODO: add declaration
				root.GetAll("xpidl-native-type-modifier").Select(m => (string)m.get_Data(0)).ToArray()
				);
			nativeType.Comment = GetComment(root);
			return nativeType;
		}

		private static XpidlTypeDef BuildTypeDef(Reduction root)
		{
			var nativeType = (Reduction)root.get_Data(1);
			return new XpidlTypeDef((string)root.get_Data(2), (string)nativeType.get_Data(0));
		}

		private static XpidlInterface BuildInterface(Reduction root)
		{
			Reduction realRoot = root;
			root = realRoot;
			string comment = GetComment(root);

			root = root.GetSiblings("xpidl-interface").First();

			string internalInterfaceComment = null;
			if (root.Parent.Handle()[1].Name() == "xpidl-comment-list")
			{
				internalInterfaceComment = GetComment(root);
			}
			XpidlInterface iface;
			string baseName = null;
			if (internalInterfaceComment == null)
			{
				if (root.Parent.Handle()[4].Name() == "xpidl-id")
					baseName = (string)root.get_Data(4);

				iface = new XpidlInterface((string)root.get_Data(2), baseName, FindIntrefaceGuid((Reduction)root.get_Data(0)), FindInterfaceModifiers((Reduction)root.get_Data(0)).ToArray());
			}
			else
			{
				if (root.Parent.Handle()[5].Name() == "xpidl-id")
					baseName = (string)root.get_Data(5);

				iface = new XpidlInterface((string)root.get_Data(3), baseName, FindIntrefaceGuid((Reduction)root.get_Data(0)), FindInterfaceModifiers((Reduction)root.get_Data(0)).ToArray());
				comment = internalInterfaceComment;
			}
			if (!Regex.IsMatch(iface.Name, @"^[a-zA-Z0-9_]+$"))
				throw new InvalidOperationException();

			if(comment != null)
				iface.Comment = new XpidlComment(comment);

			Reduction interfaceMembersReducton = root.GetSiblings("xpidl-interface-members").FirstOrDefault();
			if (interfaceMembersReducton != null)
			{
				foreach (Reduction reduction in interfaceMembersReducton.GetAll("xpidl-interface-member-decl"))
				{
					XpidlNamedNode member = null;
					Reduction memberReducton = (Reduction)reduction.GetSiblings("xpidl-interface-member").First().get_Data(0);
					switch (memberReducton.Parent.Head().Name())
					{
						case "xpidl-constant":
							member = BuildConstant(memberReducton);
							break;
						case "xpidl-attribute":
							member = BuildAttribute(memberReducton);
							break;
						case "xpidl-method":
							member = BuildMethod(memberReducton);
							break;
					}
					if (member != null)
					{
						Reduction commentsReduction = reduction.GetSiblings("xpidl-comment-list").FirstOrDefault();
						if (commentsReduction != null)
						{
							IEnumerable<Reduction> comments = commentsReduction.GetAll("xpidl-comment").ToArray();
							int count = comments.Count();
							if (count > 2)
							{
								comments = comments.Skip(count - 2);
							}
							comment = (string)comments.Last().get_Data(0);
							if (comment.StartsWith("/*"))
							{
								member.Comment = new XpidlComment(comment);
								var code = (string)comments.First().get_Data(0);
								if (code.StartsWith("%{"))
								{
									member.AddNode(new XpidlInlineCHeader(comment));
								}
							}
							else if(comment.StartsWith("%{"))
							{
								member.AddNode(new XpidlInlineCHeader(comment));
							}
						}
						iface.AddNode(member);
					}
				}
			}

			return iface;
		}

		private static XpidlAttribute BuildAttribute(Reduction root)
		{
			var attr = new XpidlAttribute(
				(string)root.GetAll("attribute-name").First().get_Data(0),
				(string)root.GetAll("xpidl-type").First().get_Data(0),
				root.GetSiblings().Contains("readonly"),
				FindMethodModifiers(root.GetAll("xpidl-method-modifiers-decl").FirstOrDefault()).ToArray());
			return attr;
		}

		private static XpidlMethod BuildMethod(Reduction root)
		{
			var exceptionsList = new List<string>();
			foreach(Reduction raisesReduction in root.GetAll("xpidl-method-raises"))
			{
				foreach (Reduction raisesList in raisesReduction.GetAll("xpidl-method-exceptions-list"))
				{
					exceptionsList.Add((string)raisesList.get_Data(raisesList.Count() - 1));
				}
			}

			var parameters = new List<XpidlMethodParameter>();
			Reduction paramList = root.GetSiblings("xpidl-params-list").FirstOrDefault();
			if (paramList != null)
			{
				foreach (Reduction param in paramList.GetAll("xpidl-param"))
				{
					XpidlParameterDirection paramDirection = XpidlParameterDirection.Default;
					Reduction xpidlParamType = param.GetSiblings("xpidl-param-type").FirstOrDefault();
					if (xpidlParamType != null)
					{
						string paramDirectionDesc = (string)xpidlParamType.get_Data(0);
						if (paramDirectionDesc.Contains("in"))
							paramDirection |= XpidlParameterDirection.In;
						if (paramDirectionDesc.Contains("out"))
							paramDirection |= XpidlParameterDirection.Out;
					}
					parameters.Add(new XpidlMethodParameter(
							(string)param.GetSiblings("xpidl-param-name").First().get_Data(0),
							(string)param.GetSiblings("xpidl-type").First().get_Data(0),
							paramDirection,
							FindParamModifiers(param.GetAll("xpidl-param-modifiers-decl").FirstOrDefault()).ToArray()
						));
				}
			}

			var method = new XpidlMethod(
				(string)root.GetSiblings().First(s => s is string),
				(string)root.GetSiblings("xpidl-type").First().get_Data(0),
				FindMethodModifiers(root.GetAll("xpidl-method-modifiers-decl").FirstOrDefault()).ToArray(),
				parameters, exceptionsList);
			return method;
		}

		private static XpidlConstant BuildConstant(Reduction root)
		{
			var constant = new XpidlConstant(
				(string)root.get_Data(2),
				(string)((Reduction)root.get_Data(1)).get_Data(0),
				CreateExpression((Reduction)root.get_Data(4))
			);
			return constant;
		}

		private static Guid FindIntrefaceGuid(Reduction root)
		{
			for (int i = 0, count = root.Count(); i < count; i++)
			{
				var reduction = root.get_Data(i) as Reduction;
				if (reduction != null && reduction.Parent.Head().Name() == "xpidl-interface-uuid")
				{
					Guid uuid;
					if (Guid.TryParse(reduction.get_Data(2) as string, out uuid))
						return uuid;
				}
			}
			throw new XpidlParserException("UUID expected");
		}

		private static IEnumerable<XpidlModifier> FindInterfaceModifiers(Reduction root)
		{
			for (int i = 0, count = root.Count(); i < count; i++)
			{
				var reduction = root.get_Data(i) as Reduction;
				if (reduction != null)
				{
					string ruleName = reduction.Parent.Head().Name();
					if (ruleName == "xpidl-interface-modifiers-list")
					{
						foreach (XpidlModifier modifier in FindInterfaceModifiers(reduction))
							yield return modifier;
					}
					else if (ruleName == "xpidl-interface-modifier")
					{
						yield return new XpidlModifier((string)reduction.get_Data(0));
					}
				}
			}
		}

		private static IEnumerable<XpidlModifier> FindMethodModifiers(Reduction root)
		{
			if (root != null)
			{
				for (int i = 0, count = root.Count(); i < count; i++)
				{
					var reduction = root.get_Data(i) as Reduction;
					if (reduction != null)
					{
						string ruleName = reduction.Parent.Head().Name();
						if (ruleName == "xpidl-method-modifiers-list")
						{
							foreach (XpidlModifier modifier in FindMethodModifiers(reduction))
								yield return modifier;
						}
						else if (ruleName == "xpidl-method-modifier")
						{
							XpidlModifier modifier = new XpidlModifier((string)reduction.get_Data(0));
							if(modifier.Name == "binaryname")
							{
								modifier.Args = new[] { (string)reduction.get_Data(2) };
							}
							else if (reduction.Count() > 0)
							{
								modifier.Args = reduction.GetSiblings(
									"xpidl-null-conversion",
									"xpidl-undefined-conversion")
									.Select(r => (string)r.get_Data(0))
									.ToArray();
							}
							yield return modifier;
						}
					}
				}
			}
		}

		private static IEnumerable<XpidlModifier> FindParamModifiers(Reduction root)
		{
			if (root != null)
			{
				for (int i = 0, count = root.Count(); i < count; i++)
				{
					var reduction = root.get_Data(i) as Reduction;
					if (reduction != null)
					{
						string ruleName = reduction.Parent.Head().Name();
						if (ruleName == "xpidl-param-modifiers-list")
						{
							foreach (XpidlModifier modifier in FindParamModifiers(reduction))
								yield return modifier;
						}
						else if (ruleName == "xpidl-param-modifier")
						{
							XpidlModifier modifier = new XpidlModifier((string)reduction.get_Data(0));
							if (reduction.Count() > 0)
							{
								modifier.Args = reduction.GetSiblings(
									"xpidl-param-name",
									"xpidl-null-conversion",
									"xpidl-undefined-conversion")
									.Select(r => (string)r.get_Data(0))
									.ToArray();
							}
							yield return modifier;
						}
					}
				}
			}
		}
		
		private static Expression CreateExpression(Reduction expression)
		{
			switch (expression.Parent.Text())
			{
				case "<xpidl-expression> ::= <xpidl-expression-or>":
				case "<xpidl-expression-or> ::= <xpidl-expression-xor>":
				case "<xpidl-expression-xor> ::= <xpidl-expression-and>":
				case "<xpidl-expression-and> ::= <xpidl-expression-shift>":
				case "<xpidl-expression-shift> ::= <xpidl-expression-add>":
				case "<xpidl-expression-add> ::= <xpidl-expression-mult>":
				case "<xpidl-expression-mult> ::= <xpidl-expression-unary>":
				case "<xpidl-expression-unary> ::= <xpidl-expression-operand>":
					return CreateExpression((Reduction)expression.get_Data(0));

				case "<xpidl-expression-or> ::= <xpidl-expression-or> '|' <xpidl-expression-xor>":
					return Expression.Or(
						CreateExpression((Reduction)expression.get_Data(0)),
						CreateExpression((Reduction)expression.get_Data(2)));

				case "<xpidl-expression-xor> ::= <xpidl-expression-xor> '^' <xpidl-expression-and>":
					return Expression.ExclusiveOr(
						CreateExpression((Reduction)expression.get_Data(0)),
						CreateExpression((Reduction)expression.get_Data(2)));

				case "<xpidl-expression-and> ::= <xpidl-expression-and> '&' <xpidl-expression-shift>":
					return Expression.And(
						CreateExpression((Reduction)expression.get_Data(0)),
						CreateExpression((Reduction)expression.get_Data(2)));

				case "<xpidl-expression-shift> ::= <xpidl-expression-shift> '<<' <xpidl-expression-add>":
					return Expression.LeftShift(
						CreateExpression((Reduction)expression.get_Data(0)),
						CreateExpression((Reduction)expression.get_Data(2)));

				case "<xpidl-expression-shift> ::= <xpidl-expression-shift> '>>' <xpidl-expression-add>":
					return Expression.RightShift(
						CreateExpression((Reduction)expression.get_Data(0)),
						CreateExpression((Reduction)expression.get_Data(2)));

				case "<xpidl-expression-add> ::= <xpidl-expression-add> '+' <xpidl-expression-mult>":
					return Expression.Add(
						CreateExpression((Reduction)expression.get_Data(0)),
						CreateExpression((Reduction)expression.get_Data(2)));

				case "<xpidl-expression-add> ::= <xpidl-expression-add> '-' <xpidl-expression-mult>":
					return Expression.Subtract(
						CreateExpression((Reduction)expression.get_Data(0)),
						CreateExpression((Reduction)expression.get_Data(2)));

				case "<xpidl-expression-mult> ::= <xpidl-expression-mult> '*' <xpidl-expression-unary>":
					return Expression.Multiply(
						CreateExpression((Reduction)expression.get_Data(0)),
						CreateExpression((Reduction)expression.get_Data(2)));

				case "<xpidl-expression-mult> ::= <xpidl-expression-mult> '/' <xpidl-expression-unary>":
					return Expression.Divide(
						CreateExpression((Reduction)expression.get_Data(0)),
						CreateExpression((Reduction)expression.get_Data(2)));

				case "<xpidl-expression-unary> ::= - <xpidl-expression-operand>":
					return Expression.Negate(
						CreateExpression((Reduction)expression.get_Data(1)));

				case "<xpidl-expression-unary> ::= ~ <xpidl-expression-operand>":
					return Expression.Not(
						CreateExpression((Reduction)expression.get_Data(1)));

				case "<xpidl-expression-operand> ::= dec-literal":
					string stringLiteral = (string)expression.get_Data(0);
					int decValue;
					if (!int.TryParse(stringLiteral, out decValue))
						return Expression.Constant(long.Parse(stringLiteral));
					return Expression.Constant(decValue);

				case "<xpidl-expression-operand> ::= hex-literal":
					long hexValue = Convert.ToInt64((string)expression.get_Data(0), 16);
					return Expression.Constant(hexValue);

				case "<xpidl-expression-operand> ::= xpidl-id":
					string fieldName = (string)expression.get_Data(0);
					return Expression.Property(Expression.Constant(fieldName), "Length");

				case "<xpidl-expression-operand> ::= '(' <xpidl-expression> ')'":
					return CreateExpression((Reduction)expression.get_Data(1));

				default:
					throw new ArgumentException(string.Format("Invalid syntax: {0}", expression));
			}
		 
		}
		
	}
}
