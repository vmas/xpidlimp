﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XpidlImp
{
	public abstract class XpidlNamedNode : XpidlComplexNode
	{
		private readonly string _name;
		private readonly string _type;

		protected XpidlNamedNode(string name, string type)
		{
			_name = name;
			_type = type;
		}

		public string Name
		{
			get { return _name; }
		}

		public string Type
		{
			get { return _type; }
		}

		public XpidlComment Comment { get; set; }

		public void AddNode(XpidlNode node)
		{
			AddNodeImpl(node);
		}
	}
}
