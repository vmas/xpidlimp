﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XpidlImp
{
	public sealed class XpidlModifier
	{
		private readonly string _name;

		public XpidlModifier(string name)
		{
			_name = name;
		}

		public string Name
		{
			get { return _name; }
		}

		public string[] Args { get; internal set; }

	}
}
