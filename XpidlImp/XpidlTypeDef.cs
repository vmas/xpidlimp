﻿using System;

namespace XpidlImp
{
	public sealed class XpidlTypeDef : XpidlNode
	{
		private readonly string _name;
		private readonly string _type;
		
		internal XpidlTypeDef(string name, string type)
		{
			_name = name;
			_type = type;
		}

		public string Name
		{
			get { return _name; }
		}

		public string Type
		{
			get { return _type; }
		}

		public override string ToString()
		{
			return string.Format("typedef {0} {1};", Type, Name);
		}

	}
}
