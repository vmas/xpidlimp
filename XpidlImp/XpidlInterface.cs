﻿using System;

namespace XpidlImp
{
	public sealed class XpidlInterface : XpidlComplexNode
	{
		private readonly string _name;
		private readonly string _baseName;
		private readonly Guid _uuid;
		private readonly XpidlModifier[] _modifiers;

		internal XpidlInterface(string name, string baseName, Guid uuid, XpidlModifier[] modifiers)
		{
			_name = name;
			_baseName = baseName;
			_uuid = uuid;
			_modifiers = modifiers;
		}

		public string Name
		{
			get { return _name; }
		}

		public string BaseName
		{
			get { return _baseName; }
		}

		public Guid Uuid
		{
			get { return _uuid; }
		}

		public XpidlModifier[] Modifiers
		{
			get { return _modifiers; }
		}

		internal void AddNode(XpidlNode node)
		{
			AddNodeImpl(node);
		}
	
	}
}
