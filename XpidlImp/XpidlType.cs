﻿using System;

namespace XpidlImp
{
	public static class XpidlType
	{
		public const string nsISupports = "nsISupports";
		public const string Void = "void";
		public const string Boolean = "boolean";
		public const string Octet = "octet";
		public const string Short = "short";
		public const string Long = "long";
		public const string LongLong = "long long";
		public const string UnsignedShort = "unsigned short";
		public const string UnsignedLong = "unsigned long";
		public const string UnsignedLongLong = "unsigned long long";
		public const string Float = "float";
		public const string Double = "double";
		public const string Char = "char";
		public const string WChar = "wchar";
		public const string String = "string";
		public const string WString = "wstring";
		public const string AString = "AString";
		public const string ACString = "ACString";
		public const string AUTF8String = "AUTF8String";
		public const string DOMString = "DOMString";
		public const string PRBool = "PRBool";
		public const string PRUint8 = "PRUint8";
		public const string PRInt16 = "PRInt16";
		public const string PRInt32 = "PRInt32";
		public const string PRInt64 = "PRInt64";
		public const string PRUint16 = "PRUint16";
		public const string PRUnichar = "PRUnichar";
		public const string PRUint32 = "PRUint32";
		public const string PRUint64 = "PRUint64";
		public const string PRTime = "PRTime";
		public const string nsRefCnt = "nsrefcnt";
		public const string nsResult = "nsresult";
		public const string nsQIResult = "nsQIResult";
		public const string nsIDRef = "nsIDRef";
		public const string nsIIDRef = "nsIIDRef";
		public const string nsCIDRef = "nsCIDRef";
		public const string nsIDPtr = "nsIDPtr";
		public const string nsIIDPtr = "nsIIDPtr";
		public const string nsCIDPtr = "nsCIDPtr";
		public const string nsNonConstIDPtr = "nsNonConstIDPtr";
		public const string nsIIDPtrShared = "nsIIDPtrShared";
		public const string ConstNsIDPtr = "const_nsID_ptr";
		public const string nsID = "nsID";
		public const string nsIID = "nsIID";
		public const string nsCID = "nsCID";
		public const string VoidPtr = "voidPtr";
		public const string JSVal = "jsval";
		public const string Bool = "bool";
		public const string UInt8t = "uint8_t";
		public const string UInt32t = "uint32_t";
		public const string UInt16t = "uint16_t";
		public const string UInt64t = "uint64_t";
		public const string Int8t = "int8_t";
		public const string Int32t = "int32_t";
		public const string Int16t = "int16_t";
		public const string Int64t = "int64_t";
		public const string Size_t = "size_t";
		public const string Char16t = "char16_t";
		public const string DOMTimeStamp = "DOMTimeStamp";
		public const string DOMHighResTimeStamp = "DOMHighResTimeStamp";
	}
}
