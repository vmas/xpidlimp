﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace XpidlImp
{
	public sealed class XpidlComment : XpidlNode
	{
		private static readonly char[] SplitChars = new char[] { ' ', '\t', '\n', '\r' };

		private readonly string _rawText;
		private readonly Dictionary<string, string> _data;
		
		internal XpidlComment(string commentText)
		{
			_rawText = commentText ?? "//";
			_data = new Dictionary<string, string>();
			Parse();
		}
	
		public bool IsSingleline
		{
			get { return RawText.StartsWith(@"//"); }
		}

		public string RawText
		{
			get { return _rawText; }
		}

		private void Parse()
		{
			string tag = "@summary";
			
			var sb = new StringBuilder();
			foreach(string line in RawText.Split('\n'))
			{
				string s = line.Trim(' ', '/', '*', '\t');
				if(s.StartsWith("@"))
				{
					_data[tag] = sb.ToString().Trim();
					sb = new StringBuilder();
					string[] tagDef = s.Split(SplitChars, 2);
					tag = tagDef[0];

					if (tag == "@param" || tag == "@throws")
					{
						tagDef = s.Split(SplitChars, 3, StringSplitOptions.RemoveEmptyEntries);
						if (tagDef.Length >= 2)
						{
							tag = tag + " " + tagDef[1];
							if (tagDef.Length == 3)
								tagDef[1] = tagDef[2];
							else
								tagDef[1] = string.Empty;
						}
					}

					s = (tagDef.Length == 1) ? string.Empty : tagDef[1];
				}
				sb.Append(" ").AppendLine(s);
			}
			_data[tag] = sb.ToString().Trim();
		}

		public string Summary
		{
			get
			{
				return _data["@summary"];
			}
		}

		public string See
		{
			get
			{
				string rv;
				return _data.TryGetValue("@see", out rv) ? rv.Trim('<', '>') : null;
			}
		}

		public string Return
		{
			get
			{
				string rv;
				return _data.TryGetValue("@return", out rv) ? rv : null;
			}
		}

		public string Deprecated
		{
			get
			{
				string rv;
				return _data.TryGetValue("@deprecated", out rv) ? rv : null;
			}
		}

		public string CommentBody
		{
			get { return IsSingleline ? RawText.Substring(2).Trim() : GetCommentBlockBody(); }
		}

		public IEnumerable<KeyValuePair<string, string>> Params
		{
			get
			{
				foreach(KeyValuePair<string, string> item in _data)
				{
					if(item.Key.StartsWith("@param"))
					{
						string[] data = item.Key.Split(SplitChars, 2, StringSplitOptions.RemoveEmptyEntries);
						if(data.Length > 1)
						{
							yield return new KeyValuePair<string, string>(data[1], item.Value);
						}
					}
				}
			}
		}

		public IEnumerable<KeyValuePair<string, string>> Throws
		{
			get
			{
				foreach (KeyValuePair<string, string> item in _data)
				{
					if (item.Key.StartsWith("@throws"))
					{
						string[] data = item.Key.Split(SplitChars, 2, StringSplitOptions.RemoveEmptyEntries);
						if (data.Length > 1)
						{
							yield return new KeyValuePair<string, string>(data[1], item.Value);
						}
					}
				}
			}
		}


		private string GetCommentBlockBody()
		{
			//TODO: Get body of multiline comment
			return RawText;
		}

		
	}
}
